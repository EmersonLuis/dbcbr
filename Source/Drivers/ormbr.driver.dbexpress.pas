{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.driver.dbexpress;

interface

uses
  Classes,
  DB,
  SqlExpr,
  Variants,
  SysUtils,
  /// orm
  ormbr.driver.connection,
  ormbr.factory.interfaces,
  ormbr.types.database,
  ormbr.utils;

type
  /// <summary>
  /// Classe de conex�o concreta com dbExpress
  /// </summary>
  TDriverDBExpress = class(TDriverConnection)
  protected
    FConnection: TSQLConnection;
    FSQLScript: TSQLQuery;
  public
    constructor Create(AConnection: TComponent; ADriverName: TDriverName); override;
    destructor Destroy; override;
    procedure Connect; override;
    procedure Disconnect; override;
    procedure ExecuteDirect(const ASQL: string); overload; override;
    procedure ExecuteDirect(const ASQL: string; const AParams: TParams); overload; override;
    procedure ExecuteScript(const ASQL: string); override;
    procedure AddScript(const ASQL: string); override;
    procedure ExecuteScripts; override;
    function IsConnected: Boolean; override;
    function InTransaction: Boolean; override;
    function CreateQuery: IDBQuery; override;
    function CreateResultSet: IDBResultSet; override;
    function ExecuteSQL(const ASQL: string): IDBResultSet; override;
  end;

  TDriverQueryDBExpress = class(TDriverQuery)
  private
    FSQLQuery: TSQLQuery;
  protected
    procedure SetCommandText(ACommandText: string); override;
    function GetCommandText: string; override;
  public
    constructor Create(AConnection: TSQLConnection);
    destructor Destroy; override;
    procedure ExecuteDirect; override;
    function ExecuteQuery: IDBResultSet; override;
  end;

  TDriverResultSetDBExpress = class(TDriverResultSet<TSQLQuery>)
  public
    constructor Create(ADataSet: TSQLQuery); override;
    destructor Destroy; override;
    function NotEof: Boolean; override;
    function GetFieldValue(AFieldName: string): Variant; overload; override;
    function GetFieldValue(AFieldIndex: Integer): Variant; overload; override;
    function GetFieldType(AFieldName: string): TFieldType; overload; override;
  end;

implementation

{ TDriverDBExpress }

constructor TDriverDBExpress.Create(AConnection: TComponent; ADriverName: TDriverName);
begin
  inherited;
  FConnection := AConnection as TSQLConnection;
  FDriverName := ADriverName;
  FSQLScript := TSQLQuery.Create(nil);
  try
    FSQLScript.SQLConnection := FConnection;
  except
    FSQLScript.Free;
    raise;
  end;
end;

destructor TDriverDBExpress.Destroy;
begin
  FConnection := nil;
  FSQLScript.Free;
  inherited;
end;

procedure TDriverDBExpress.Disconnect;
begin
  inherited;
  FConnection.Connected := False;
end;

procedure TDriverDBExpress.ExecuteDirect(const ASQL: string);
begin
  inherited;
  FConnection.ExecuteDirect(ASQL);
end;

procedure TDriverDBExpress.ExecuteDirect(const ASQL: string; const AParams: TParams);
var
  oExeSQL: TSQLQuery;
  iFor: Integer;
begin
  oExeSQL := TSQLQuery.Create(nil);
  try
    oExeSQL.SQLConnection := FConnection;
    oExeSQL.SQL.Text := ASQL;
    for iFor := 0 to AParams.Count - 1 do
    begin
      oExeSQL.Params[iFor].DataType := AParams[iFor].DataType;
      oExeSQL.Params[iFor].Value    := AParams[iFor].Value;
    end;
    try
      oExeSQL.ExecSQL;
    except
      raise;
    end;
  finally
    oExeSQL.Free;
  end;
end;

procedure TDriverDBExpress.ExecuteScript(const ASQL: string);
begin
  inherited;
  FSQLScript.SQL.Text := ASQL;
  FSQLScript.ExecSQL;
end;

procedure TDriverDBExpress.ExecuteScripts;
begin
  inherited;
  try
    FSQLScript.ExecSQL;
  finally
    FSQLScript.SQL.Clear;
  end;
end;

function TDriverDBExpress.ExecuteSQL(const ASQL: string): IDBResultSet;
var
  oDBQuery: IDBQuery;
begin
  oDBQuery := TDriverQueryDBExpress.Create(FConnection);
  oDBQuery.CommandText := ASQL;
  Result := oDBQuery.ExecuteQuery;
end;

procedure TDriverDBExpress.AddScript(const ASQL: string);
begin
  inherited;
  FSQLScript.SQL.Add(ASQL);
end;

procedure TDriverDBExpress.Connect;
begin
  inherited;
  FConnection.Connected := True;
end;

function TDriverDBExpress.InTransaction: Boolean;
begin
  inherited;
  Result := FConnection.InTransaction;
end;

function TDriverDBExpress.IsConnected: Boolean;
begin
  inherited;
  Result := FConnection.Connected = True;
end;

function TDriverDBExpress.CreateQuery: IDBQuery;
begin
  Result := TDriverQueryDBExpress.Create(FConnection);
end;

function TDriverDBExpress.CreateResultSet: IDBResultSet;
var
  oDBQuery: IDBQuery;
begin
  oDBQuery := TDriverQueryDBExpress.Create(FConnection);
  Result   := oDBQuery.ExecuteQuery;
end;

{ TDriverDBExpressQuery }

constructor TDriverQueryDBExpress.Create(AConnection: TSQLConnection);
begin
  if AConnection <> nil then
  begin
     FSQLQuery := TSQLQuery.Create(nil);
     try
       FSQLQuery.SQLConnection := AConnection;
     except
       FSQLQuery.Free;
       raise;
     end;
  end;
end;

destructor TDriverQueryDBExpress.Destroy;
begin
  FSQLQuery.Free;
  inherited;
end;

function TDriverQueryDBExpress.ExecuteQuery: IDBResultSet;
var
  oResultSet: TSQLQuery;
  iFor: Integer;
begin
  oResultSet := TSQLQuery.Create(nil);
  try
    oResultSet.SQLConnection := FSQLQuery.SQLConnection;
    oResultSet.SQL.Text := FSQLQuery.CommandText;

    for iFor := 0 to FSQLQuery.Params.Count - 1 do
    begin
      oResultSet.Params[iFor].DataType := FSQLQuery.Params[iFor].DataType;
      oResultSet.Params[iFor].Value    := FSQLQuery.Params[iFor].Value;
    end;
    oResultSet.Open;
  except
    oResultSet.Free;
    raise;
  end;
  Result := TDriverResultSetDBExpress.Create(oResultSet);
  /// <summary>
  /// if oResultSet.RecordCount = 0 then
  /// Ao checar Recordcount no DBXExpress da um erro de Object Inv�lid para o SQL
  /// select name as name, ' ' as description from sys.sequences
  /// </summary>
  if oResultSet.Eof then
     Result.FetchingAll := True;
end;

function TDriverQueryDBExpress.GetCommandText: string;
begin
  Result := FSQLQuery.CommandText;
end;

procedure TDriverQueryDBExpress.SetCommandText(ACommandText: string);
begin
  inherited;
  FSQLQuery.CommandText := ACommandText;
end;

procedure TDriverQueryDBExpress.ExecuteDirect;
begin
  FSQLQuery.ExecSQL;
end;

{ TDriverResultSetDBExpress }

constructor TDriverResultSetDBExpress.Create(ADataSet: TSQLQuery);
begin
  FDataSet := ADataSet;
  inherited;
end;

destructor TDriverResultSetDBExpress.Destroy;
begin
  FDataSet.Free;
  inherited;
end;

function TDriverResultSetDBExpress.GetFieldValue(AFieldName: string): Variant;
var
  oField: TField;
begin
  oField := FDataSet.FieldByName(AFieldName);
  Result := GetFieldValue(oField.Index);
end;

function TDriverResultSetDBExpress.GetFieldType(AFieldName: string): TFieldType;
begin
  Result := FDataSet.FieldByName(AFieldName).DataType;
end;

function TDriverResultSetDBExpress.GetFieldValue(AFieldIndex: Integer): Variant;
var
  LValue: Variant;
begin
  if AFieldIndex > FDataSet.FieldCount -1  then
    Exit(Variants.Null);

  if FDataSet.Fields[AFieldIndex].IsNull then
    Result := Variants.Null
  else
  begin
    LValue := FDataSet.Fields[AFieldIndex].Value;
    /// <summary>
    /// Usando DBExpress para acessar SQLite os campos data retornam no
    /// formato ISO8601 "yyyy-MM-dd e o DBExpress n�o converte para dd-MM-yyy,
    /// ent�o tive que criar uma alternativa.
    /// </summary>
    if FDataSet.SQLConnection.DriverName = 'Sqlite' then
      if (Copy(LValue,5,1) = '-') and (Copy(LValue,8,1) = '-') then
      begin
         Result := TUtilSingleton.GetInstance.Iso8601ToDateTime(LValue);
         Exit;
      end;
    Result := LValue;
  end;
end;

function TDriverResultSetDBExpress.NotEof: Boolean;
begin
  if not FFirstNext then
     FFirstNext := True
  else
     FDataSet.Next;

  Result := not FDataSet.Eof;
end;

end.