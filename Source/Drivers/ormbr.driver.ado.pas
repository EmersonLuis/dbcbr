{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.driver.ado;

interface

uses
  Classes,
  SysUtils,
  DB,
  Variants,
  ADODB,
  /// orm
  ormbr.driver.connection,
  ormbr.types.database,
  ormbr.factory.interfaces;

type
  /// <summary>
  /// Classe de conex�o concreta com dbExpress
  /// </summary>
  TDriverADO = class(TDriverConnection)
  protected
    FConnection: TADOConnection;
    FSQLScript: TADOQuery;
  public
    constructor Create(AConnection: TComponent; ADriverName: TDriverName); override;
    destructor Destroy; override;
    procedure Connect; override;
    procedure Disconnect; override;
    procedure ExecuteDirect(const ASQL: string); overload; override;
    procedure ExecuteDirect(const ASQL: string; const AParams: TParams); overload; override;
    procedure ExecuteScript(const ASQL: string); override;
    procedure AddScript(const ASQL: string); override;
    procedure ExecuteScripts; override;
    function IsConnected: Boolean; override;
    function InTransaction: Boolean; override;
    function CreateQuery: IDBQuery; override;
    function CreateResultSet: IDBResultSet; override;
    function ExecuteSQL(const ASQL: string): IDBResultSet; override;
  end;

  TDriverQueryADO = class(TDriverQuery)
  private
    FSQLQuery: TADOQuery;
  protected
    procedure SetCommandText(ACommandText: string); override;
    function GetCommandText: string; override;
  public
    constructor Create(AConnection: TADOConnection);
    destructor Destroy; override;
    procedure ExecuteDirect; override;
    function ExecuteQuery: IDBResultSet; override;
  end;

  TDriverResultSetADO = class(TDriverResultSet<TADOQuery>)
  public
    constructor Create(ADataSet: TADOQuery); override;
    destructor Destroy; override;
    function NotEof: Boolean; override;
    function GetFieldValue(AFieldName: string): Variant; overload; override;
    function GetFieldValue(AFieldIndex: Integer): Variant; overload; override;
    function GetFieldType(AFieldName: string): TFieldType; overload; override;
  end;

implementation

{ TDriverADO }

constructor TDriverADO.Create(AConnection: TComponent; ADriverName: TDriverName);
begin
  inherited;
  FConnection := AConnection as TADOConnection;
  FDriverName := ADriverName;
  FSQLScript := TADOQuery.Create(nil);
  try
    FSQLScript.Connection := FConnection;
  except
    FSQLScript.Free;
    raise;
  end;
end;

destructor TDriverADO.Destroy;
begin
  FConnection := nil;
  FSQLScript.Free;
  inherited;
end;

procedure TDriverADO.Disconnect;
begin
  inherited;
  FConnection.Connected := False;
end;

procedure TDriverADO.ExecuteDirect(const ASQL: string);
begin
  inherited;
  FConnection.Execute(ASQL);
end;

procedure TDriverADO.ExecuteDirect(const ASQL: string; const AParams: TParams);
var
  oExeSQL: TADOQuery;
  iFor: Integer;
begin
  oExeSQL := TADOQuery.Create(nil);
  try
    oExeSQL.Connection := FConnection;
    oExeSQL.SQL.Text   := ASQL;
    for iFor := 0 to AParams.Count - 1 do
    begin
      oExeSQL.Parameters[iFor].DataType := AParams[iFor].DataType;
      oExeSQL.Parameters[iFor].Value    := AParams[iFor].Value;
    end;
    try
      oExeSQL.ExecSQL;
    except
      raise;
    end;
  finally
    oExeSQL.Free;
  end;
end;

procedure TDriverADO.ExecuteScript(const ASQL: string);
begin
  inherited;
  FSQLScript.SQL.Text := ASQL;
  FSQLScript.ExecSQL;
end;

procedure TDriverADO.ExecuteScripts;
begin
  inherited;
  try
    FSQLScript.ExecSQL;
  finally
    FSQLScript.SQL.Clear;
  end;
end;

function TDriverADO.ExecuteSQL(const ASQL: string): IDBResultSet;
var
  oDBQuery: IDBQuery;
begin
  oDBQuery := TDriverQueryADO.Create(FConnection);
  oDBQuery.CommandText := ASQL;
  Result := oDBQuery.ExecuteQuery;
end;

procedure TDriverADO.AddScript(const ASQL: string);
begin
  inherited;
  FSQLScript.SQL.Add(ASQL);
end;

procedure TDriverADO.Connect;
begin
  inherited;
  FConnection.Connected := True;
end;

function TDriverADO.InTransaction: Boolean;
begin
  inherited;
  Result := FConnection.InTransaction;
end;

function TDriverADO.IsConnected: Boolean;
begin
  inherited;
  Result := FConnection.Connected = True;
end;

function TDriverADO.CreateQuery: IDBQuery;
begin
  Result := TDriverQueryADO.Create(FConnection);
end;

function TDriverADO.CreateResultSet: IDBResultSet;
var
  oDBQuery: IDBQuery;
begin
  oDBQuery := TDriverQueryADO.Create(FConnection);
  Result   := oDBQuery.ExecuteQuery;
end;

{ TDriverDBExpressQuery }

constructor TDriverQueryADO.Create(AConnection: TADOConnection);
begin
  if AConnection <> nil then
  begin
     FSQLQuery := TADOQuery.Create(nil);
     try
       FSQLQuery.Connection := AConnection;
     except
       FSQLQuery.Free;
       raise;
     end;
  end;
end;

destructor TDriverQueryADO.Destroy;
begin
  FSQLQuery.Free;
  inherited;
end;

function TDriverQueryADO.ExecuteQuery: IDBResultSet;
var
  oResultSet: TADOQuery;
  iFor: Integer;
begin
  oResultSet := TADOQuery.Create(nil);
  try
    oResultSet.Connection := FSQLQuery.Connection;
    oResultSet.SQL.Text := FSQLQuery.SQL.Text;

    for iFor := 0 to FSQLQuery.Parameters.Count - 1 do
    begin
      oResultSet.Parameters[iFor].DataType := FSQLQuery.Parameters[iFor].DataType;
      oResultSet.Parameters[iFor].Value    := FSQLQuery.Parameters[iFor].Value;
    end;
    oResultSet.Open;
  except
    oResultSet.Free;
    raise;
  end;
  Result := TDriverResultSetADO.Create(oResultSet);
  if oResultSet.RecordCount = 0 then
     Result.FetchingAll := True;
end;

function TDriverQueryADO.GetCommandText: string;
begin
  Result := FSQLQuery.SQL.Text;
end;

procedure TDriverQueryADO.SetCommandText(ACommandText: string);
begin
  inherited;
  FSQLQuery.SQL.Text := ACommandText;
end;

procedure TDriverQueryADO.ExecuteDirect;
begin
  FSQLQuery.ExecSQL;
end;

{ TDriverResultSetADO }

constructor TDriverResultSetADO.Create(ADataSet: TADOQuery);
begin
  FDataSet:= ADataSet;
  inherited;
end;

destructor TDriverResultSetADO.Destroy;
begin
  FDataSet.Free;
  inherited;
end;

function TDriverResultSetADO.GetFieldValue(AFieldName: string): Variant;
var
  oField: TField;
begin
  oField := FDataSet.FieldByName(AFieldName);
  Result := GetFieldValue(oField.Index);
end;

function TDriverResultSetADO.GetFieldType(AFieldName: string): TFieldType;
begin
  Result := FDataSet.FieldByName(AFieldName).DataType;
end;

function TDriverResultSetADO.GetFieldValue(AFieldIndex: Integer): Variant;
begin
  if AFieldIndex > FDataSet.FieldCount -1  then
    Exit(Variants.Null);

  if FDataSet.Fields[AFieldIndex].IsNull then
     Result := Variants.Null
  else
     Result := FDataSet.Fields[AFieldIndex].Value;
end;

function TDriverResultSetADO.NotEof: Boolean;
begin
  if not FFirstNext then
     FFirstNext := True
  else
     FDataSet.Next;

  Result := not FDataSet.Eof;
end;

end.
